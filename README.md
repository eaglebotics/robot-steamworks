# FRC 2017 FIRST Steamworks

FRC Team 5410 (Eaglebotics) Robot Code

Dependencies/Libraries:
NBPSEaglebotics/dependencies

# EGit (must be installed to access repository using Eclipse)
https://eclipse.org/egit/

# Commiting and using EGit
To commit using eGit (staging changes, pushing, etc.), ask me in person.

# Class Path Errors
When you first run the program in Eclipse, it will not build since the class path is incorrect. Follow these steps to resolve this issue (these may vary per system):

1. In the section down and to the right of the file navigator click on "Problems" (if not already selected)

2. Click on the arrow next to the red X.

3. Right click on one of the errors where the build path is incorrect.

4. Click "Quick Fix."

5. Click "Finish" (this doesn't actually fix the problem, but instead gets us to the screen where we can fix the problem)

6. Click on "Libraries"

7. Find the library with the red X.

8. Click on the arrow to the left of the library with the build error

9. Click on the file with the build path error (look to the right of its name for more information about the error)

10. Click "Edit."

11. Select the actual location of the .jar file

12. Click "Apply"

13. Rinse and repeat for all dependencies that have build path errors.


Also, please ignore Github identifying this code as mostly HTML. This is due to the fact that navx-mxp has a lot of HTML files in it (in the parts we don't use).
