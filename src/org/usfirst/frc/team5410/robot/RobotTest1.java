package org.usfirst.frc.team5410.robot;

import edu.wpi.cscore.CvSink;
import edu.wpi.cscore.CvSource;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoSink;
import edu.wpi.first.wpilibj.CameraServer;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.vision.VisionRunner;
import edu.wpi.first.wpilibj.vision.VisionThread;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import com.ctre.CANTalon;

public class RobotTest1 extends IterativeRobot {
	final String defaultAuto = "Default";
	final String customAuto = "My Auto";
	String autoSelected;
	SendableChooser<String> chooser = new SendableChooser<>();
	
	//Global Variables for use within other methods
	private static CANTalon Motor1 = new CANTalon(1);	//Motor 1 which is connected to ...
	private static CANTalon Motor2 = new CANTalon(2);
	private static CANTalon Motor3 = new CANTalon(3);
	private static CANTalon Motor4 = new CANTalon(4);
	private static CANTalon Motor5 = new CANTalon(5);
	private static CANTalon Motor6 = new CANTalon(6);
	private static CANTalon Motor7 = new CANTalon(7);
	private static CANTalon Motor8 = new CANTalon(8);
	private static CANTalon Motor9 = new CANTalon(9);
	private static Joystick Joystick1 = new Joystick(0);
	private Thread visionThread;
	private String output;
	
	
	@Override
	public void robotInit() {
		chooser.addDefault("Default Auto", defaultAuto);
		chooser.addObject("My Auto", customAuto);
		SmartDashboard.putData("Auto choices", chooser);
		visionThread = new Thread(() -> {
			GripPipeline Grip = new GripPipeline();
			UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
			camera.setResolution(1280, 720);
			camera.setExposureManual(10);
			CvSink cvs = CameraServer.getInstance().getVideo();
			CvSource OutHSL = CameraServer.getInstance().putVideo("HSL Output", 1280, 720);
			Mat image = new Mat();
			while(!Thread.interrupted()){
				if (cvs.grabFrame(image) == 0) {
					OutHSL.notifyError(cvs.getError());
					continue;
				}
				OutHSL.putFrame(image);
				if (!Grip.filterContoursOutput().isEmpty()) {
					output = "Yes!";
				}
				else {
					output = "No.";
				}
		}});
		visionThread.setDaemon(true);
		visionThread.start();
	}
	
	@Override
	public void autonomousInit() {
		autoSelected = chooser.getSelected();
		// autoSelected = SmartDashboard.getString("Auto Selector",
		// defaultAuto);
		System.out.println("Auto selected: " + autoSelected);
	}

	
	@Override
	public void autonomousPeriodic() {
		switch (autoSelected) {
		case customAuto:
			SmartDashboard.putString("Tape Detected:", output);
			break;
		case defaultAuto:
		default:
			System.out.println("Default Auto Complete.");
			break;
		}
	}

	
	@Override
	public void teleopPeriodic() {
		
		Motor1.set(Joystick1.getY());
		Motor2.set(-Joystick1.getY());
	}

	
	@Override
	public void testPeriodic() {
	}
	
	
	public void teleopinit()
	{
		
	}
	
}
