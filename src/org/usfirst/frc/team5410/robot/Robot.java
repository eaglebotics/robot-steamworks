package org.usfirst.frc.team5410.robot;
import edu.wpi.cscore.CvSource;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.first.wpilibj.CameraServer;
import com.ctre.CANTalon;
import edu.wpi.first.wpilibj.GamepadBase;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.vision.VisionRunner;
import edu.wpi.first.wpilibj.vision.VisionThread;

import java.awt.Color;
import java.util.ArrayList;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.SPI;


public class Robot extends IterativeRobot {
	SendableChooser chooser = new SendableChooser<>();//Used to store the command we want
	static AHRS gyro; //Instantiate Gyro
	static CANTalon leftTop = new CANTalon(28);//Instantiate Motors
	static CANTalon leftBot = new CANTalon(30);
	static CANTalon rightTop = new CANTalon(20);
	static CANTalon rightBot = new CANTalon(21);
//	static CANTalon leftTop = new CANTalon(1);//Instantiate Motors
//	static CANTalon leftBot = new CANTalon(2);
//	static CANTalon rightTop = new CANTalon(3);
//	static CANTalon rightBot = new CANTalon(4);
	CANTalon side = new CANTalon(22);	
	CANTalon gear = new CANTalon(26);
	CANTalon shooter1 = new CANTalon(23);
	CANTalon shooter2 = new CANTalon(24);
	CANTalon climber1 = new CANTalon(29);
	CANTalon climber2 = new CANTalon(31);
	CANTalon pickUp = new CANTalon(27);
	CANTalon agitator = new CANTalon(25);
	final Point center = new Point(180,120);

	boolean inPrev = false;

	final int gearDistance = 800;
	boolean didAuto;

	
	
	static RobotDrive Drive = new RobotDrive(leftBot,leftTop,rightBot,rightTop);//A robot drive for controlling the motors which drive the robot. The pattern being 1 and 2 are on the left side and 3 and 4 on the other

	static Joystick joystick1 = new Joystick(0); //Instantiate Joysticks
	static Joystick joystick2 = new Joystick(1);
	
	private VisionThread Grip;//Instantiate Vision Thread
	private VisionThread Grip2;//Instantiate Vision Thread
	
	String output = "";
	String coords = "";
	
	static ArrayList<MatOfPoint> contours;//Array for contour data sent by Vision Thread
	Rect[] r;//Array of rectangles for uploading to image
	int[] midpoint = new int[2];
	Point visionCenter;
	Mat image = new Mat();//Instantiate images
	Mat Analyzed = new Mat();
	final double WHEELRPM = 2000;//RPM for shooter
	int driveTicks;
	float distMoved;

	@Override
	public void robotInit() {
		leftTop.setFeedbackDevice(FeedbackDevice.QuadEncoder);//Setting Encoder Settings
		leftTop.configEncoderCodesPerRev(80);

		rightTop.setFeedbackDevice(FeedbackDevice.QuadEncoder);
		rightTop.configEncoderCodesPerRev(80);
		
//		rightTop.setInverted(true);
//		rightBot.setInverted(true);
//		leftTop.setInverted(true);
//		leftBot.setInverted(true);
		
		
		
		gear.setFeedbackDevice(FeedbackDevice.QuadEncoder);//Motor Settings
		gear.reverseOutput(true);
		gear.reverseSensor(false);
		gear.configEncoderCodesPerRev(420);
		gear.setProfile(0);//Used for positioning
		gear.setF(0.1);
		gear.setP(0.7);
		gear.setI(0.001);
		gear.setD(0.1);
		gear.configNominalOutputVoltage(+0.0f, -0.0f);
		gear.configPeakOutputVoltage(+12.0f, -12.0f);
		gear.setForwardSoftLimit(5);
		gear.setReverseSoftLimit(5);
		gear.setPosition(0);
		gear.changeControlMode(TalonControlMode.Position);
		shooter1.setFeedbackDevice(FeedbackDevice.QuadEncoder);
		shooter1.reverseSensor(true);
		shooter1.configEncoderCodesPerRev(1024);
		shooter1.setProfile(0);
		shooter1.setF(0.1);
		shooter1.setP(0.1);
		shooter1.setI(0);
		shooter1.setD(0);
		shooter1.configNominalOutputVoltage(+0.0f, -0.0f);
		shooter1.configPeakOutputVoltage(+12.0f, -12.0f);
		shooter1.setPosition(0);
		shooter1.setForwardSoftLimit(15);
		shooter1.setReverseSoftLimit(15);
		shooter1.changeControlMode(TalonControlMode.Speed);
		shooter2.changeControlMode(TalonControlMode.Follower);
		shooter2.set(shooter1.getDeviceID());
		climber1.changeControlMode(TalonControlMode.PercentVbus);
		climber2.changeControlMode(TalonControlMode.Follower);
		climber2.set(climber1.getDeviceID());
//		side.setFeedbackDevice(FeedbackDevice.QuadEncoder);
//		side.reverseSensor(false);
//		side.configEncoderCodesPerRev(80);
//		side.setProfile(0);
//		side.setF(0.0624);
//		side.setP(1);
//		side.setI(0);
//		side.setD(0);
//		side.configNominalOutputVoltage(+0.0f, -0.0f);
//		side.configPeakOutputVoltage(+12.0f, -12.0f);
//		side.setPosition(0);
//		side.setForwardSoftLimit(15);
//		side.setReverseSoftLimit(15);
//		side.changeControlMode(TalonControlMode.Speed);
		UsbCamera camera1 = CameraServer.getInstance().startAutomaticCapture(0);
		UsbCamera camera2 = CameraServer.getInstance().startAutomaticCapture(1);
		camera1.setResolution(640, 360);
		camera1.setExposureManual(-10);
		camera2.setResolution(640, 360);
		camera2.setExposureManual(-10);
		CvSource OutFin = CameraServer.getInstance().putVideo("Analyzed", 360, 240);
		Grip = new VisionThread(new VisionRunner<GripPipeline>(camera1, new GripPipeline(), pipeline -> {
			image = pipeline.resizeImageOutput();
			contours = pipeline.filterContoursOutput();
			r = new Rect[contours.size()];
			coords = "";
			for(int i = 0; (i < contours.size() && i < 2); i++){
				r[i] = Imgproc.boundingRect(contours.get(i));
				coords += ("Rectangle " + i + ": [Top Left: " + r[i].tl().toString() + "], [Bottom Right: " + r[i].br().toString()) + "] \n";
			}
			
			Imgproc.applyColorMap(image, image, 11);
			for(int i = 0; (i < r.length && i < 2); i++){
				Imgproc.rectangle(image, r[i].tl(), r[i].br(), new Scalar(255, 105, 150, 255), 5);
			}
			visionCenter = new Point(midpoint[0],midpoint[1]);
			if (r.length == 2){
				midpoint[0] = (int) ((r[0].tl().x + r[1].br().x) / 2);
				midpoint[1] = (int) ((r[0].tl().y + r[1].br().y) / 2);
				Imgproc.circle(image, visionCenter, 3, new Scalar(0, 0, 255, 255), 3);
			}
			else{
				midpoint[0] = 0;
				midpoint[1] = 0;
			}
			if ((visionCenter.x - center.x) > 10){
				Imgproc.arrowedLine(image, new Point(270, 200), new Point(340, 200), new Scalar(180, 105, 255, 255), 3, 8, 0, 0.1);
			}
			else if ((visionCenter.x - center.x) < -10){
				Imgproc.arrowedLine(image, new Point(160, 200), new Point(90, 200), new Scalar(180, 105, 255, 255), 3, 8, 0, 0.1);
			}
			else{
				Imgproc.putText(image, ":)", new Point(180, 60), 1, 2, new Scalar(255, 255, 0, 255), 3);
			}
			Imgproc.putText(image, "5410", new Point(40,40), 1, 3, new Scalar(255, 0, 0, 255), 3);
			Imgproc.circle(image, new Point(180, 240), 42, new Scalar(0, 0, 255, 255), 6);
			Imgproc.circle(image, new Point(180, 240), 36, new Scalar(71, 99, 255, 255), 6);
			Imgproc.circle(image, new Point(180, 240), 30, new Scalar(0, 255, 255, 255), 6);
			Imgproc.circle(image, new Point(180, 240), 24, new Scalar(0, 0, 255, 0), 6);
			Imgproc.circle(image, new Point(180, 240), 18, new Scalar(255, 0, 0, 255), 6);
			Imgproc.circle(image, new Point(180, 240), 12, new Scalar(255, 0, 255, 255), 6);
			Analyzed = image;
			OutFin.putFrame(Analyzed);
			if (!pipeline.filterContoursOutput().isEmpty()) {
				output = "Success!";
			}
			else {
				output = "Failed.";
			}
		}));
		Grip.setPriority(Thread.MIN_PRIORITY);//seems fine... if the error robot drive is not updated enough shows up though this is taking up too much processing power
		Grip.setDaemon(true);
		Grip.start();
		gyro = new AHRS(SPI.Port.kMXP);
		gyro.reset();
		Grip.yield();
		
//		Grip2 = new VisionThread(new VisionRunner<GripPipeline>(camera2, new GripPipeline(), pipeline -> {
//			image = pipeline.resizeImageOutput();
//			contours = pipeline.filterContoursOutput();
//			r = new Rect[contours.size()];
//			coords = "";
//			for(int i = 0; (i < contours.size() && i < 2); i++){
//				r[i] = Imgproc.boundingRect(contours.get(i));
//				coords += ("Rectangle " + i + ": [Top Left: " + r[i].tl().toString() + "], [Bottom Right: " + r[i].br().toString()) + "] \n";
//			}
//			
//			Imgproc.applyColorMap(image, image, 11);
//			for(int i = 0; (i < r.length && i < 2); i++){
//				Imgproc.rectangle(image, r[i].tl(), r[i].br(), new Scalar(255, 105, 150, 255), 5);
//			}
//			visionCenter = new Point(midpoint[0],midpoint[1]);
//			if (r.length == 2){
//				midpoint[0] = (int) ((r[0].tl().x + r[1].br().x) / 2);
//				midpoint[1] = (int) ((r[0].tl().y + r[1].br().y) / 2);
//				Imgproc.circle(image, visionCenter, 3, new Scalar(0, 0, 255, 255), 3);
//			}
//			else{
//				midpoint[0] = 0;
//				midpoint[1] = 0;
//			}
//			if ((visionCenter.x - center.x) > 10){
//				Imgproc.arrowedLine(image, new Point(270, 200), new Point(340, 200), new Scalar(180, 105, 255, 255), 3, 8, 0, 0.1);
//			}
//			else if ((visionCenter.x - center.x) < -10){
//				Imgproc.arrowedLine(image, new Point(160, 200), new Point(90, 200), new Scalar(180, 105, 255, 255), 3, 8, 0, 0.1);
//			}
//			else{
//				Imgproc.putText(image, ":)", new Point(180, 60), 1, 2, new Scalar(255, 255, 0, 255), 3);
//			}
//			Imgproc.putText(image, "5410", new Point(40,40), 1, 3, new Scalar(255, 0, 0, 255), 3);
//			Imgproc.circle(image, new Point(180, 240), 42, new Scalar(0, 0, 255, 255), 6);
//			Imgproc.circle(image, new Point(180, 240), 36, new Scalar(71, 99, 255, 255), 6);
//			Imgproc.circle(image, new Point(180, 240), 30, new Scalar(0, 255, 255, 255), 6);
//			Imgproc.circle(image, new Point(180, 240), 24, new Scalar(0, 0, 255, 0), 6);
//			Imgproc.circle(image, new Point(180, 240), 18, new Scalar(255, 0, 0, 255), 6);
//			Imgproc.circle(image, new Point(180, 240), 12, new Scalar(255, 0, 255, 255), 6);
//			Analyzed = image;
//			OutFin.putFrame(Analyzed);
//			if (!pipeline.filterContoursOutput().isEmpty()) {
//				output = "Success!";
//			}
//			else {
//				output = "Failed.";
//			}
//		}));
//		Grip2.setPriority(Thread.MIN_PRIORITY);//seems fine... if the error robot drive is not updated enough shows up though this is taking up too much processing power
//		//Grip.setDaemon(true);
//		gyro = new AHRS(SPI.Port.kMXP);
//		gyro.reset();
	}
	
	@Override
	public void autonomousInit() {
		didAuto = false;
	}

	
	@Override
	public void autonomousPeriodic() {
			SmartDashboard.putString("Vision Detection:", output);
			SmartDashboard.putString("Contour Coords:", coords);
			SmartDashboard.putString("Midpoint:", midpoint[0] +", " + midpoint[1]);
			SmartDashboard.putNumber("Distance in the x", gyro.getDisplacementX());
			SmartDashboard.putNumber("Distance in the y", gyro.getDisplacementY());
			SmartDashboard.putNumber("Distance in the z", gyro.getDisplacementZ());
			
			SmartDashboard.putNumber("vision Center", visionCenter.x);
			SmartDashboard.putNumber("center", center.x);
			driveTicks = (rightTop.getEncPosition() + leftTop.getEncPosition()) / 2;
			distMoved = gyro.getDisplacementX();
			if (didAuto == false){
//				if (Timer.getMatchTime() < 1){
//					Drive.tankDrive(0.6, 0.6);
//				}
//				else{
//					Drive.tankDrive(0.55, 0.55);
//					gear.set(-0.625);
//					Timer.delay(0.2);
//					Drive.tankDrive(-0.6, -0.6);
//					gear.set(0);
//					Timer.delay(2);
//					didAuto = true;
					//Add rotation later
//				}
				if (Math.abs(visionCenter.x - center.x) > 10){
					if (visionCenter.x == 0){
						side.set(0);
					}
					else if (visionCenter.x > center.x){
						side.set(-0.5);
					}
					else if (visionCenter.x < center.x){
						side.set(0.5);
					}
				}
				else{
					side.set(0);
				}
			}
	}

	
	@Override
	public void teleopPeriodic() {
			
			if(Math.abs(joystick1.getRawAxis(4)) > 0.1)
				{
				
				Drive.tankDrive(deadZone(joystick1.getRawAxis(4)) * -1, deadZone(joystick1.getRawAxis(4)), true) ;
				gyro.reset();
				}
			else if (gyro.getAngle() > 0 && joystick1.getRawAxis(1) > 0)
				Drive.tankDrive(deadZone(joystick1.getRawAxis(1)) - deadZone(joystick1.getRawAxis(1) * (gyro.getAngle() /360)*10), deadZone(joystick1.getRawAxis(1)), true);//Drives the robot			
			
			else if (gyro.getAngle() < 0 && joystick1.getRawAxis(1) > 0)
				Drive.tankDrive(deadZone(joystick1.getRawAxis(1)),  deadZone(joystick1.getRawAxis(1) +  deadZone(joystick1.getRawAxis(1)) * (gyro.getAngle() / 360)*10), true);//Drives the robot
			
			else if (gyro.getAngle() > 0 && joystick1.getRawAxis(1) < 0)
				Drive.tankDrive(deadZone(joystick1.getRawAxis(1)) + deadZone(joystick1.getRawAxis(1) * (gyro.getAngle() /360)*10), deadZone(joystick1.getRawAxis(1)), true);//Drives the robot			
			
			else if (gyro.getAngle() < 0 && joystick1.getRawAxis(1) < 0)
				Drive.tankDrive(deadZone(joystick1.getRawAxis(1)),  deadZone(joystick1.getRawAxis(1) -  deadZone(joystick1.getRawAxis(1)) * (gyro.getAngle() / 360)*10), true);//Drives the robot
			side.set(deadZone(joystick1.getRawAxis(0)));
			if (gyro.getAngle() > 0)SmartDashboard.putNumber("Value for speed lost", deadZone(joystick1.getRawAxis(1) -  deadZone(joystick1.getRawAxis(1)) * (gyro.getAngle() / 360)));
			else if (gyro.getAngle() < 0)SmartDashboard.putNumber("Value for speed lost", deadZone(joystick1.getRawAxis(1) +  deadZone(joystick1.getRawAxis(1)) * (gyro.getAngle() / 360)));
			
		//Drive.tankDrive(joystick1.getRawAxis(1), 0);
			SmartDashboard.putNumber("Angle", gyro.getAngle());
					
		if (joystick2.getRawButton(1)){
			gear.set(-0.625);
		}
		else if (joystick2.getRawButton(2)){
			gear.set(-0.125);
		}
		else{
			gear.set(0);
		}
		//joystick1.setRumble(RumbleType.kLeftRumble, 1);
		shooter1.changeControlMode(TalonControlMode.PercentVbus);
		//shooter1.set(deadZone(joystick1.getRawAxis(3)));
		if(Math.abs(joystick1.getRawAxis(3)) > 0){
			shooter1.set(0.62);
			agitator.set(100);
		}
		else{
			shooter1.set(0);
			agitator.set(0);
		}
		if (joystick1.getRawButton(4)){
			pickUp.set(1);
		}
		else{
			pickUp.set(0);
		}
		//shooter1.set(deadZone(SmartDashboard.getDouble("Shooter Speed")));
		climber1.set(deadZone(joystick2.getRawAxis(3)));
		SmartDashboard.putNumber("joystick1", joystick1.getRawAxis(1));//for debugging
		SmartDashboard.putNumber("joystick2", joystick1.getRawAxis(5));//for debugging
		SmartDashboard.putNumber("Out 1:", leftTop.getOutputVoltage() / leftTop.getBusVoltage());
		SmartDashboard.putNumber("Out 2:", rightTop.getOutputVoltage() / rightTop.getBusVoltage());
		SmartDashboard.putNumber("Speed 1:", leftTop.getSpeed());
		SmartDashboard.putNumber("Speed 2:", rightTop.getSpeed());
		SmartDashboard.putNumber("Shooter Speed", joystick2.getRawAxis(3) * 7000);
		SmartDashboard.putNumber("Encoder Position 1:", leftTop.getEncPosition());
		SmartDashboard.putNumber("Encoder Position 2:", rightTop.getEncPosition());
		SmartDashboard.putNumber("Encoder Position 3:", gear.getEncPosition());
		SmartDashboard.putString("1st value of contours", coords);
		SmartDashboard.putNumber("Distance in the x", gyro.getDisplacementX());
		SmartDashboard.putNumber("Distance in the y", gyro.getDisplacementY());
		SmartDashboard.putNumber("Distance in the z", gyro.getDisplacementZ());
		
		
		if (joystick2.getRawButton(6))
		{
			Grip.stop();
			Grip2.start();
		}
		else if (joystick2.getRawButton(5))
		{
			Grip2.stop();
			Grip.start();
		}
		
		
	}
	
	public static double deadZone(double x){
		if (Math.abs(x) > 0.05) return(x);
		return(0);
	}

	
	@Override
	public void testPeriodic() {
	}
	
	
	public void teleopinit()
	{
		
	}
	
}

